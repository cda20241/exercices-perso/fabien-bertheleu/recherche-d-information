import random

INDEX_PRENOM = 0
INDEX_NOM = 1
INDEX_AGE = 2


#je créé la liste originale avec une liste à 2 dimensions (une liste de listes)
liste_originale = [
        ["Pascal", "ROSE", 43],
        ["Mickaël", "FLEUR", 29],
        ["Henri", "TULIPE", 35],
        ["Michel", "FRAMBOISE", 35],
        ["Arthur", "PETALE", 35],
        ["Michel", "POLLEN", 50],
        ["Michel", "FRAMBOISE", 42]
    ]

#je melange aléatoirement la liste
random.shuffle(liste_originale)

#je recherche les personnes qui s'appellent Michel ou qui ont 50 ans
for prenom in liste_originale:
    if prenom[0] == "Michel" or prenom[2] >= 50 :
        print("Exercie 1 \n On a dans la liste quelqu'un qui adore le camping")
        break

#j'énumère les personnes qui aiment le camping
liste_amateurs=[]
for element in liste_originale:
    if element[0] == "Michel" or element[2] >=50:
        liste_amateurs.append(element)

print("Exercice 2 \n C'est formidable, ", liste_amateurs, "adore le camping")

#verifification du nombre de lettres dans le prénom et le nom
for longueur in liste_originale
    if len(liste_originale, 0) = len(liste_originale, 1):
        print()
